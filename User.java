package com.example.employeedatamysql;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String EmployeeName;

    private Integer Age;

    private Integer DateOfBirth;

    private String ProjectName;

    private String EmployerName;

    private Integer Salary;

    private Integer YearsOfExperience;

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String EmployeeName) {
        this.EmployeeName = EmployeeName;
    }

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer Age) {
        this.Age = Age;
    }



    public Integer getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(Integer dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getProjectName() {
        return ProjectName;
    }

    public void setProjectName(String projectName) {
        ProjectName = projectName;
    }

    public String getEmployerName() {
        return EmployerName;
    }

    public void setEmployerName(String employerName) {
        EmployerName = employerName;
    }

    public Integer getSalary() {
        return Salary;
    }

    public void setSalary(Integer salary) {
        Salary = salary;
    }

    public Integer getYearsOfExperience() {
        return YearsOfExperience;
    }

    public void setYearsOfExperience(Integer yearsOfExperience) {
        YearsOfExperience = yearsOfExperience;
    }


}



