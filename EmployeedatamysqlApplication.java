package com.example.employeedatamysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeedatamysqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeedatamysqlApplication.class, args);
    }

}
